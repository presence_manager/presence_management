import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render( < App / > , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

//****styled component, react-bootstrap  */

/* --------------------------------------------------------------------------------------------------\\*
||*************-----------------***************---------------***************************************||
 -- this project is created by abdellatif Ahammad Zakaria Mahmoud Elkhazenti Aymen--
 --*-1- rule:1 =>you have to keep this comment her until the end of project 
 --*-2- rule:2 => you have to respect the time work and the structure of project(!use .jsx not .js)
 --*-- rule:3 => Now !!! the files : -
                                      |-=>actions for actions if we need to redux
                                      |-=>Views: her you can put the views(.jsx) non static can  change
                                      |-=>assets: every img svg videos you use must be here only
                                      |-=>components: the dummy parts of the page it's can be navs...
                                      |-=>reducer : for redux

*/