import React from 'react';

import styled from 'styled-components'
import Form from 'react-bootstrap/Form'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'

const Containers = styled.div`
position: absolute;
width: 100%;
background: linear-gradient(270deg, #46B5F3 -3.53%, rgba(73, 176, 234, 0) 101.88%), #A4E3F1;
 box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);

`;

const SiteName = styled.h3`
color: #FFFFFF;

`;

const LoginButton = styled.h3`
float:right;
width: 128px;
height: 37px;
margin: 14px;
background: #FFFFFF;
border: 2px solid #FFFFFF;
box-sizing: border-box;
border-radius: 50px;
`;

const LoginLinc = styled.a`
float:right;
width: 90px;
font-family: Poiret One;
font-style: normal;
font-weight: normal;
font-size: 24px;
line-height: 28px;
color: #43B7E9;

`;



const ButtonRegister = styled.div`

float:right;
width: 128px;
height: 37px;
margin: 13px;
background: #5EB1FF;
border: 2px solid #FFFFFF;
box-sizing: border-box;
border-radius: 50px;
`;


const RegisterLinc = styled.a`
float:right;
width: 100px;
font-family: Poiret One;
font-style: normal;
font-weight: normal;
font-size: 24px;
line-height: 28px;
color: #FFFFFF;
`;

function NavBar() {
    return(
     
<Containers>
<Navbar expand="lg">
  <Navbar.Brand  href="#home">
    <SiteName>projet absence  </SiteName>
  </Navbar.Brand>

  <Navbar.Toggle aria-controls="basic-navbar-nav"/>
  <Navbar.Collapse id="basic-navbar-nav">
    <Nav className="mr-auto">
    </Nav>
    <Form inline>
            <ButtonRegister>
         <RegisterLinc href="#" >
              Register
          </RegisterLinc>
      </ButtonRegister>

      <LoginButton>

          <LoginLinc href="#">
              Login
          </LoginLinc>

      </LoginButton>
    </Form>
    </Navbar.Collapse>
</Navbar>
</Containers>

    );


}

export default NavBar;
