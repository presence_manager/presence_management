import React from 'react';
import styled from "styled-components";

import {Container,Row,Col,Form} from 'react-bootstrap';

const ButtonLogin =styled.a`

position: static;
width: 180px;
height: 35px;
margin-left:150px;
font-family: "Times New Roman", Times, serif;
font-size: 30px;
background: linear-gradient(90deg, #4D9FFF 0%, rgba(255, 255, 255, 0) 70.12%), #6DD3FF;
border-radius: 50px;
text-align: center;
line-height: 35px;

`

function Header(){
return(

    <Container>
     <div className="center">
     <Row>
    <Col  >
    <div className="Imagelogin"></div>
   
    </Col>
    <Col >
<div className="divLogin">
    <Form>
  <Form.Group controlId="formBasicEmail">
  <h2>Login</h2>
    <Form.Control type="email" placeholder="Enter email" className="Input"/>
  </Form.Group>

  <Form.Group controlId="formBasicPassword">
    
    <Form.Control type="password" placeholder="Password" className="Input"/>
  </Form.Group>
  
  <ButtonLogin variant="primary" type="submit" >
    Submit
  </ButtonLogin>
    <div><a className="url" href="www.example.com">Mot de passe oublié ?</a></div>
</Form>
</div>
    </Col>
    
  </Row>
  </div>
    </Container>
   

);

}
export default Header;